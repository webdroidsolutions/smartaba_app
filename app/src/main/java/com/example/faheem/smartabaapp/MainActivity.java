package com.example.faheem.smartabaapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.LocationManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.webkit.GeolocationPermissions;
import android.support.v4.content.ContextCompat;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.Manifest;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.*;

import android.content.SharedPreferences;

public class MainActivity extends AppCompatActivity implements LocationListener{

    protected LocationManager locationManager;
    double latitude  = 999999;
    double longitude = 999999;
    double altitude = 999999;
    String Phone;
    String Password;
    long Time;

    TextView ErrorMessage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Intent intent = getIntent();
        if (intent.hasExtra("status")) {
            String Status = intent.getStringExtra("status");

            ErrorMessage = (TextView) findViewById(R.id.textViewStatusMessage);
            if (Status.equals("reset"))
                ErrorMessage.setText("Password anda telah diganti");
            if (Status.equals("logout"))
                ErrorMessage.setText("Anda Sudah LogOut");
            if (Status.equals("expired"))
                ErrorMessage.setText("Tidak Ada Aktivitas Selama 30 Menit");
            ErrorMessage.setVisibility(View.VISIBLE);

            intent.removeExtra("status");
        }

        ErrorMessage = (TextView) findViewById(R.id.textViewErrorMessage);

        statusCheck();

        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) ==
                PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) ==
                        PackageManager.PERMISSION_GRANTED) {
        } else {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, 2);
        }

        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5*60*1000, 100, this);
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 5*60*1000, 100, this);

        SharedPreferences prefs = getSharedPreferences("UserData", MODE_PRIVATE);
        String UserID = prefs.getString("UserID", "test");
        String LoginID = prefs.getString("LoginID", "test");
        String Phone = prefs.getString("Phone", "test");
        String Token = prefs.getString("Token", "test");

        if (!UserID.equals("test") && !LoginID.equals("test") && !Phone.equals("test") && !Token.equals("test")) {
            RequestQueue queue = Volley.newRequestQueue(this);
            String url = "https://www.moneypivot.com/smartaba/check_session.php";

            StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            // Display the first 500 characters of the response string.
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String ErrorCode = jsonObject.getString("ErrorCode");
                                Log.e("------------------", ErrorCode);
                                if (ErrorCode.equals("000")) {
                                    Intent i = new Intent(MainActivity.this, TestActivity.class);
                                    startActivity(i);
                                    finish();
                                } else if (ErrorCode.equals("002")) {
                                    SharedPreferences prefs = getSharedPreferences("UserData", MODE_PRIVATE);
                                    SharedPreferences.Editor edit = prefs.edit();
                                    edit.clear();
                                    edit.apply();

                                    ErrorMessage.setText("Tidak Ada Aktivitas Selama 30 Menit");
                                    ErrorMessage.setVisibility(View.VISIBLE);
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() {
                    SharedPreferences prefs = getSharedPreferences("UserData", MODE_PRIVATE);
                    String UserID = prefs.getString("UserID", "test");
                    String LoginID = prefs.getString("LoginID", "test");
                    String Phone = prefs.getString("Phone", "test");
                    String Token = prefs.getString("Token", "test");

                    Map<String, String> params = new HashMap<String, String>();
                    params.put("UserID", UserID + "");
                    params.put("LoginID", LoginID + "");
                    params.put("Phone", Phone + "");
                    params.put("Token", Token + "");

                    return params;
                }
            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                    5000,
                    1,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            queue.add(stringRequest);
        }
    }

    @Override
        public void onLocationChanged(Location location) {
        latitude = location.getLatitude();
        longitude = location.getLongitude();
        altitude = location.getAltitude();
//        ErrorMessage.setText(latitude+" - "+longitude);
//        ErrorMessage.setVisibility(View.VISIBLE);
    }

    @Override
    public void onProviderDisabled(String provider) {
        latitude  = 999999;
        longitude = 999999;
        altitude = 999999;
        buildAlertMessageNoGps();
    }

    @Override
    public void onProviderEnabled(String provider) {
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
    }

    public void statusCheck() {
        final LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            buildAlertMessageNoGps();

        }
    }

    private void buildAlertMessageNoGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Your GPS seems to be disabled, do you want to enable it?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }

    public void submitForm(View v) {
        ErrorMessage.setVisibility(View.GONE);

        Phone = ((EditText) findViewById(R.id.editTextPhone)).getText().toString();
        Password = ((EditText) findViewById(R.id.editTextPassword)).getText().toString();
        Time = System.currentTimeMillis() / 1000L;

        if (latitude == 999999) {
            Toast.makeText(getBaseContext(), "Please wait, waiting to get your location", Toast.LENGTH_SHORT).show();
            return;
        }

        if (Phone.equals("")) {
            Toast.makeText(getBaseContext(), "Nomor Handphone Belum Diisi", Toast.LENGTH_LONG).show();
            return;
        }

        if (!Phone.toString().matches("^\\+62\\d+$")) {
            Toast.makeText(getBaseContext(), "Nomor Handphone Salah\nHarus Dengan Kode Negara\nContoh +6288980006000", Toast.LENGTH_LONG).show();
            return;
        }

        if (Password.equals("")) {
            Toast.makeText(getBaseContext(), "Password Belum Diisi", Toast.LENGTH_LONG).show();
            return;
        }

        if (Password.length() < 6) {
            Toast.makeText(getBaseContext(), "Password Minimal 6 Karakter", Toast.LENGTH_LONG).show();
            return;
        }

        RequestQueue queue = Volley.newRequestQueue(this);
        String url = "https://www.moneypivot.com/smartaba/login.php";

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the first 500 characters of the response string.
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String ErrorCode = jsonObject.getString("ErrorCode");
                            Log.e("------------------", ErrorCode);
                            if (ErrorCode.equals("010")) {
                                ErrorMessage.setText("Mohon Atur Waktu Di Handphone/Tablet Anda Dengan Benar");
                                ErrorMessage.setVisibility(View.VISIBLE);
                            }
                            else if (ErrorCode.equals("001")) {
                                ErrorMessage.setText("Nomor Handphone Dan/Atau Password Salah");
                                ErrorMessage.setVisibility(View.VISIBLE);
                            }
                            else if (ErrorCode.equals("000")) {
                                SharedPreferences.Editor editor = getSharedPreferences("UserData", MODE_PRIVATE).edit();
                                editor.putString("UserID", jsonObject.getString("UserID"));
                                editor.putString("LoginID", jsonObject.getString("LoginID"));
                                editor.putString("Phone", jsonObject.getString("Phone"));
                                editor.putString("Token", jsonObject.getString("Token"));
                                editor.apply();

                                Intent i = new Intent(MainActivity.this, TestActivity.class);
                                startActivity(i);
                                finish();
                            }
                            else {
                                Toast.makeText(getBaseContext(), "Something went wrong, please try again", Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            Toast.makeText(getBaseContext(), "Something went wrong, please try again", Toast.LENGTH_SHORT).show();
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this)
                                .setTitle("No Internet")
                                .setMessage("Connect To Internet and try again")
                                .setCancelable(false)
                                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                    }
                                }).create();
                        alertDialog.show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("phone", Phone);
                params.put("password", Password);
                params.put("lat", latitude + "");
                params.put("long", longitude + "");
                params.put("alt", altitude + "");
                params.put("time", Time + "");

                return params;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                5000,
                1,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        queue.add(stringRequest);
    }

    public void navigateForgot(View v) {
        Intent i = new Intent(MainActivity.this, ForgotPasswordActivity.class);
        startActivity(i);
    }
}