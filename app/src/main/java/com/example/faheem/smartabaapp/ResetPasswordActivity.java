package com.example.faheem.smartabaapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.LocationManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.webkit.GeolocationPermissions;
import android.support.v4.content.ContextCompat;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.Manifest;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.*;
import android.os.CountDownTimer;

public class ResetPasswordActivity extends AppCompatActivity implements LocationListener{

    String ResetID;
    protected LocationManager locationManager;
    double latitude  = 999999;
    double longitude = 999999;
    double altitude = 999999;
    String Password;
    String Code;
    long Time;

    TextView ErrorMessage;
    TextView ResendSMS;

    int[] SMS_Durations = {300,     900,        1800,       3600,       21600,  43200,  86400};
    int[] SMS_Intervals = {1000,    1000,       1000,       1000,       6000,   6000,   6000};
    String[] SMS_Units =  {"menit", "menit",    "menit",    "menit",    "jam",  "jam",  "jam"};
    int SMS_CurrentIndex = 0;
    int SMS_Duration;
    int SMS_Interval;
    String SMS_Unit;
    int SMS_CurrentTime = 0;
    int SMS_DurationLeft;

    CountDownTimer timer;
//    var Times = [
//    {'Duration': 300, 'Interval': 1000, 'Unit': 'menit'},
//    {'Duration': 900, 'Interval': 1000, 'Unit': 'menit'},
//    {'Duration': 1800, 'Interval': 1000, 'Unit': 'menit'},
//    {'Duration': 3600, 'Interval': 1000, 'Unit': 'menit'},
//    {'Duration': 21600, 'Interval': 60000, 'Unit': 'jam'},
//    {'Duration': 43200, 'Interval': 60000, 'Unit': 'jam'},
//    {'Duration': 86400, 'Interval': 60000, 'Unit': 'jam'},
//            ];
//
//    var CurrentTime = 0;
//    var DurationLeft = Times[CurrentTime]['Duration'];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password);

        Intent intent = getIntent();
        ResetID = intent.getStringExtra("ResetID");

        ErrorMessage = (TextView) findViewById(R.id.textViewErrorMessage);
        ResendSMS = (TextView) findViewById(R.id.textViewResend);

        statusCheck();

        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) ==
                PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) ==
                        PackageManager.PERMISSION_GRANTED) {
        } else {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, 2);
        }

        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);

        incrementTime();
    }

    @Override
    public void onLocationChanged(Location location) {
        latitude = location.getLatitude();
        longitude = location.getLongitude();
        altitude = location.getAltitude();
    }

    @Override
    public void onProviderDisabled(String provider) {
        buildAlertMessageNoGps();
    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
    }

    public void statusCheck() {
        final LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            buildAlertMessageNoGps();

        }
    }

    private void buildAlertMessageNoGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Your GPS seems to be disabled, do you want to enable it?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }

    public void submitForm(View v) {
        ErrorMessage.setVisibility(View.GONE);

        Password = ((EditText) findViewById(R.id.editTextPassword)).getText().toString();
        String ConfirmPassword = ((EditText) findViewById(R.id.editTextPasswordConfirm)).getText().toString();
        Code = ((EditText) findViewById(R.id.editTextCode)).getText().toString();
        Time = System.currentTimeMillis() / 1000L;

        if (latitude == 999999) {
            Toast.makeText(getBaseContext(), "Please wait, waiting to get your location", Toast.LENGTH_LONG).show();
            return;
        }

        if (Password.equals("") || ConfirmPassword.equals("")) {
            Toast.makeText(getBaseContext(), "Password Belum Diisi", Toast.LENGTH_LONG).show();
            return;
        }

        if (!Password.equals(ConfirmPassword)) {
            Toast.makeText(getBaseContext(), "Kata Sandi Tidak Cocok", Toast.LENGTH_LONG).show();
            return;
        }

        if (Password.length() < 6) {
            Toast.makeText(getBaseContext(), "Password Minimal 6 Karakter", Toast.LENGTH_LONG).show();
            return;
        }

        if (Code.equals("")) {
            Toast.makeText(getBaseContext(), "Kode Ganti Password Belum Diisi", Toast.LENGTH_LONG).show();
            return;
        }

        if (Code.length() != 6) {
            Toast.makeText(getBaseContext(), "Kode Ganti Password Kurang", Toast.LENGTH_LONG).show();
            return;
        }

        RequestQueue queue = Volley.newRequestQueue(this);
        String url = "https://www.moneypivot.com/smartaba/gantiReset.php";

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the first 500 characters of the response string.
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String ErrorCode = jsonObject.getString("ErrorCode");
                            Log.e("------------------", ErrorCode);
                            if (ErrorCode.equals("010")) {
                                ErrorMessage.setText("Mohon Atur Waktu Di Handphone/Tablet Anda Dengan Benar");
                                ErrorMessage.setVisibility(View.VISIBLE);
                            }
                            else if (ErrorCode.equals("001")) {
                                ErrorMessage.setText("Kode Password Salah");
                                ErrorMessage.setVisibility(View.VISIBLE);
                            }
                            else if (ErrorCode.equals("002")) {
                                ErrorMessage.setText("Kode Ganti Password Salah");
                                ErrorMessage.setVisibility(View.VISIBLE);
                            }
                            else if (ErrorCode.equals("000")) {
                                Intent i = new Intent(ResetPasswordActivity.this, MainActivity.class);
                                i.putExtra("status", "reset");
                                startActivity(i);
                                finish();
                            }
                            else {
                                Toast.makeText(getBaseContext(), "Something went wrong, please try again", Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            Toast.makeText(getBaseContext(), "Something went wrong, please try again", Toast.LENGTH_LONG).show();
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        AlertDialog alertDialog = new AlertDialog.Builder(ResetPasswordActivity.this)
                                .setTitle("No Internet")
                                .setMessage("Connect To Internet and try again")
                                .setCancelable(false)
                                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                    }
                                }).create();
                        alertDialog.show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("id", ResetID);
                params.put("password", Password);
                params.put("password_confirm", Password);
                params.put("code", Code);
                params.put("lat", latitude + "");
                params.put("long", longitude + "");
                params.put("alt", altitude + "");
                params.put("time", Time + "");

                return params;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                5000,
                1,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        queue.add(stringRequest);
    }

    public void updateTime() {
        int hours = (int)(Math.floor((SMS_DurationLeft % (60 * 60 * 24)) / (60 * 60)));
        int minutes = (int)(Math.floor((SMS_DurationLeft % (60 * 60)) / (60)));
        int seconds = (int)(Math.floor((SMS_DurationLeft % (60))));
        String new_hours = (hours < 10 ? "0"+hours : hours+"");
        String new_minutes = (minutes < 10 ? "0"+minutes : minutes+"");
        String new_seconds = (seconds < 10 ? "0"+seconds : seconds+"");

        if (SMS_Unit.equals("menit"))
            ResendSMS.setText("Kirim Lagi SMS Dalam "+new_minutes+":"+new_seconds+" "+SMS_Unit);
        else
            ResendSMS.setText("Kirim Lagi SMS Dalam "+new_hours+":"+new_minutes+" "+SMS_Unit);
    }

    public void incrementTime() {
        if (SMS_CurrentIndex < 7) {
            SMS_Duration = SMS_DurationLeft = SMS_Durations[SMS_CurrentIndex];
            SMS_Interval = SMS_Intervals[SMS_CurrentIndex];
            SMS_Unit = SMS_Units[SMS_CurrentIndex];

            if (SMS_CurrentIndex != 0)
                timer.cancel();

            timer = new CountDownTimer(SMS_Duration * 1000, SMS_Interval) {
                public void onTick(long millisUntilFinished) {
                    SMS_DurationLeft -= SMS_Interval / 1000;
                    updateTime();
                }

                public void onFinish() {
                    Intent i = new Intent(ResetPasswordActivity.this, MainActivity.class);
                    startActivity(i);
                    finish();
                }
            };

            timer.start();
            SMS_CurrentIndex++;
        }
    }

    public void resendSMS(View v) {
        RequestQueue queue = Volley.newRequestQueue(this);
        String url = "https://www.moneypivot.com/smartaba/resend_sms.php?id="+ResetID;

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the first 500 characters of the response string.
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String ErrorCode = jsonObject.getString("ErrorCode");
                            Log.e("------------------", ErrorCode);
                            if (ErrorCode.equals("000")) {
                                Toast.makeText(getBaseContext(), "SMS berhasil dikirim", Toast.LENGTH_LONG).show();
                                incrementTime();
                            }
                            else {
                                Toast.makeText(getBaseContext(), "Something went wrong, please try again", Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            Toast.makeText(getBaseContext(), "Something went wrong, please try again", Toast.LENGTH_LONG).show();
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        AlertDialog alertDialog = new AlertDialog.Builder(ResetPasswordActivity.this)
                                .setTitle("No Internet")
                                .setMessage("Connect To Internet and try again")
                                .setCancelable(false)
                                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                    }
                                }).create();
                        alertDialog.show();
                    }
                });
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                5000,
                1,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        queue.add(stringRequest);
    }
}