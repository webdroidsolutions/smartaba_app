package com.example.faheem.smartabaapp;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.webkit.WebChromeClient;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import android.content.SharedPreferences;

import im.delight.android.webview.AdvancedWebView;

public class TestActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, AdvancedWebView.Listener, LocationListener {
    private AdvancedWebView mWebView;
    protected LocationManager locationManager;
    double latitude  = 999999;
    double longitude = 999999;
    double altitude = 999999;
    String Password;
    String Code;
    long Time;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        statusCheck();

        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) ==
                PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) ==
                        PackageManager.PERMISSION_GRANTED) {
        } else {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, 2);
        }

        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5*60*1000, 100, this);
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 5*60*1000, 100, this);

        mWebView = (AdvancedWebView) findViewById(R.id.webview);
        mWebView.loadUrl("http://smartaba.kidaba.com/");
    }

    @Override
    public void onLocationChanged(Location location) {
        Time = System.currentTimeMillis() / 1000L;
        latitude = location.getLatitude();
        longitude = location.getLongitude();
        altitude = location.getAltitude();
        Log.e("================",latitude+"-"+longitude+"-"+altitude);

        RequestQueue queue = Volley.newRequestQueue(this);
        String url = "https://www.moneypivot.com/smartaba/update_location.php";

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the first 500 characters of the response string.
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String ErrorCode = jsonObject.getString("ErrorCode");
                            Log.e("------------------", ErrorCode);
                            if (!ErrorCode.equals("000")) {
                                SharedPreferences prefs = getSharedPreferences("UserData", MODE_PRIVATE);
                                SharedPreferences.Editor edit = prefs.edit();
                                edit.clear();
                                edit.apply();

                                Intent i = new Intent(TestActivity.this, MainActivity.class);
                                if (ErrorCode.equals("002")) {
                                    i.putExtra("status", "expired");
                                }
                                startActivity(i);
                                finish();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                SharedPreferences prefs = getSharedPreferences("UserData", MODE_PRIVATE);
                String UserID = prefs.getString("UserID", "test");
                String LoginID = prefs.getString("LoginID", "test");
                String Phone = prefs.getString("Phone", "test");
                String Token = prefs.getString("Token", "test");

                Map<String, String> params = new HashMap<String, String>();
                params.put("UserID", UserID + "");
                params.put("LoginID", LoginID + "");
                params.put("Phone", Phone + "");
                params.put("Token", Token + "");
                params.put("lat", latitude + "");
                params.put("long", longitude + "");
                params.put("alt", altitude + "");
                params.put("time", Time + "");

                return params;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                5000,
                1,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        queue.add(stringRequest);
    }

    @Override
    public void onProviderDisabled(String provider) {
        latitude  = 999999;
        longitude = 999999;
        altitude = 999999;
        buildAlertMessageNoGps();
    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
    }

    public void statusCheck() {
        final LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            buildAlertMessageNoGps();

        }
    }

    private void buildAlertMessageNoGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Your GPS seems to be disabled, do you want to enable it?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }

    @SuppressLint("NewApi")
    @Override
    protected void onResume() {
        super.onResume();
        mWebView.onResume();
        // ...
    }

    @SuppressLint("NewApi")
    @Override
    protected void onPause() {
        mWebView.onPause();
        // ...
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        mWebView.onDestroy();
        // ...
        super.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        mWebView.onActivityResult(requestCode, resultCode, intent);
    }

    @Override
    public void onPageStarted(String url, Bitmap favicon) {
    }

    @Override
    public void onPageFinished(String url) {
    }

    @Override
    public void onPageError(int errorCode, String description, String failingUrl) {
        mWebView.setVisibility(View.GONE);
        AlertDialog alertDialog=new AlertDialog.Builder(TestActivity.this)
                .setTitle("Connection Problem")
                .setMessage("Connect To Internet and try again")
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).create();
        alertDialog.show();
    }

    @Override
    public void onDownloadRequested(String url, String suggestedFilename, String mimeType, long contentLength, String contentDisposition, String userAgent) {

    }

    @Override
    public void onExternalPageRequest(String url) {

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.test, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_logout) {

            new AlertDialog.Builder(this)
                    .setMessage("Benar Akan LogOut?")
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setPositiveButton("Ya LogOut", new DialogInterface.OnClickListener() {

                        public void onClick(DialogInterface dialog, int whichButton) {
                            Time = System.currentTimeMillis() / 1000L;

                            RequestQueue queue = Volley.newRequestQueue(TestActivity.this);
                            String url = "https://www.moneypivot.com/smartaba/logout.php";

                            StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                                    new Response.Listener<String>() {
                                        @Override
                                        public void onResponse(String response) {
                                            // Display the first 500 characters of the response string.
                                            try {
                                                JSONObject jsonObject = new JSONObject(response);
                                                String ErrorCode = jsonObject.getString("ErrorCode");
                                                Log.e("------------------", ErrorCode);
                                                if (ErrorCode.equals("000")) {
                                                    SharedPreferences prefs = getSharedPreferences("UserData", MODE_PRIVATE);
                                                    SharedPreferences.Editor edit = prefs.edit();
                                                    edit.clear();
                                                    edit.apply();

                                                    Intent i = new Intent(TestActivity.this, MainActivity.class);
                                                    i.putExtra("status", "logout");
                                                    startActivity(i);
                                                    finish();
                                                } else {
                                                    Toast.makeText(getBaseContext(), "Something went wrong, please try again", Toast.LENGTH_LONG).show();
                                                }
                                            } catch (JSONException e) {
                                                Toast.makeText(getBaseContext(), "Something went wrong, please try again", Toast.LENGTH_LONG).show();
                                                e.printStackTrace();
                                            }
                                        }
                                    },
                                    new Response.ErrorListener() {
                                        @Override
                                        public void onErrorResponse(VolleyError error) {
                                            AlertDialog alertDialog = new AlertDialog.Builder(TestActivity.this)
                                                    .setTitle("No Internet")
                                                    .setMessage("Connect To Internet and try again")
                                                    .setCancelable(false)
                                                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                                        @Override
                                                        public void onClick(DialogInterface dialogInterface, int i) {
                                                            dialogInterface.dismiss();
                                                        }
                                                    }).create();
                                            alertDialog.show();
                                        }
                                    }) {
                                @Override
                                protected Map<String, String> getParams() {
                                    SharedPreferences prefs = getSharedPreferences("UserData", MODE_PRIVATE);
                                    String UserID = prefs.getString("UserID", "test");
                                    String LoginID = prefs.getString("LoginID", "test");
                                    String Phone = prefs.getString("Phone", "test");
                                    String Token = prefs.getString("Token", "test");

                                    Map<String, String> params = new HashMap<String, String>();
                                    params.put("UserID", UserID + "");
                                    params.put("LoginID", LoginID + "");
                                    params.put("Phone", Phone + "");
                                    params.put("Token", Token + "");
                                    params.put("lat", latitude + "");
                                    params.put("long", longitude + "");
                                    params.put("alt", altitude + "");
                                    params.put("time", Time + "");

                                    return params;
                                }
                            };
                            stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                                    5000,
                                    1,
                                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                            queue.add(stringRequest);
                        }
                    })
                    .setNegativeButton("Batal LogOut", null).show();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
